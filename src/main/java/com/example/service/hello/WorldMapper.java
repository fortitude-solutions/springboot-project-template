package com.example.service.hello;

import com.example.service.hello.api.World;
import com.example.service.hello.entity.WorldEntity;

/**
* Maps from entity to api versions and back
*/
public class WorldMapper {
  public static WorldEntity fromApi(World api) {
    WorldEntity entity = new WorldEntity();
    entity.setName(api.getName());

    return entity;
  }

  public static World fromEntity(WorldEntity entity) {
    World api = new World();
    api.setId(entity.getId());
    api.setName(entity.getName());

    return api;
  }

}
