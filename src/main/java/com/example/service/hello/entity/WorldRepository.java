package com.example.service.hello.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

/**
 * A Repostiory provides a simple abstraction for
 */
public interface WorldRepository extends JpaRepository<WorldEntity, Long> {
}
